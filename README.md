# Templates

To get started with your own templates based on OS-APS you can copy any of the examples in this repository and extend or modify them.

## Getting started

OS-APS templates come with a number of options right out of the box. These can be changed in the UI by clicking the cog wheel and then saving the manuscript. Popular options are page size, margins, or whether a running head should be used. To get started, head over to our [demo instance](https://os-aps.de/demo/) and try it yourself.

## Creating your own templates

If you are hosting your own OS-APS instance, you can easily create new templates by providing a template directory during the deployment. You can follow [this documentation](https://docs.sciflow.org/self-hosting/) to get the instance setup.

## Advanced

For more information on how to programmatically change templates, please see our [TDK docs](https://docs.sciflow.org/development/tdk/) and the [Dev setup](https://docs.sciflow.org/development/setup/).

